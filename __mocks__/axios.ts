import { readFile } from 'fs/promises';

interface Param {
  field: string
  type: string
  value: string
}

interface Response {
  headers: object
  status: number
  statusText: string
  data: string | object
}

export interface Action {
  url: string
  priority: number
  params: Param[]
  statusCode: number
  statusMessage: string
  headers: Record<string, string>
  body: string
  error?: string
  timeout?: boolean
}

export interface ActionInput {
  url: string
  priority?: number
  params?: Param[]
  statusCode?: number
  statusMessage?: string
  headers?: Record<string, string>
  body?: string
  error?: string
  timeout?: boolean
}

const defaultAction: Action = {
  url: '',
  priority: 10,
  params: [],
  statusCode: 200,
  statusMessage: 'OK',
  headers: {},
  body: '',
};

const actions: Action[] = [];

export interface HttpsMock {
  __addAction: (url: string, action: ActionInput) => void
  request: (config: AxiosRequestConfig) => string | object
}

export function __addAction (action: ActionInput): void {
  actions.push({
    ...defaultAction,
    ...action
  });
}

export async function __loadFromJson (path: string): Promise<void> {
  const buffer = await readFile(path);
  const json = JSON.parse(buffer.toString()) as ActionInput[];
  for (const action of json) {
    __addAction({ ...action, body: JSON.stringify(action.body) });
  }
}

const checkParams = {
  'exact': (action: string, req: string): boolean => {
    return action === req;
  },
  'regexp': (action: string, req: string): boolean => {
    return (new RegExp(action)).test(req);
  }
}

function matchParams (action: Param[], req: object): boolean {
  for (const param of action) {
    const actionValue = req[param.field] ?? '';
    if (!checkParams[param.type](param.value, actionValue)) {
      return false;
    }
  }
  return true;
}

interface AxiosRequestConfig {
  baseURL: string
  url: string
  params: object
  responseType: string
}

function getAction (config: AxiosRequestConfig): Action {
  const url = `${config.baseURL ?? ''}${config.url ?? ''}`;
  const sorted = actions.sort((a, b) => a.priority <= b.priority ? -1 : 1);
  for (const action of sorted) {
    if (url !== action.url) {
      continue;
    }
    if (matchParams(action.params, config.params)) {
      return action
    }
  }
  return defaultAction;
}

export async function request (config: AxiosRequestConfig): Promise<Response> {
  const action = getAction(config);
  const data = config.responseType === 'json' ? JSON.parse(action.body) : action.body;
  return {
    headers: action.headers,
    status: action.statusCode,
    statusText: action.statusMessage,
    data,
  };
}

export default {
  request
}
