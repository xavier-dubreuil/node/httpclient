import { jest } from '@jest/globals';
import * as mocks from './__mocks__';
import * as fixtures from './fixtures';

jest.mock('axios');

const mockAxios: typeof mocks.axios = jest.requireMock('axios');

for (const fixture of fixtures.axios.actions) {
  mockAxios.__addAction(fixture);
}
