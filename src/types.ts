import { AxiosRequestTransformer, AxiosResponseTransformer, AxiosRequestHeaders, AxiosResponseHeaders, RawAxiosResponseHeaders, AxiosRequestConfig } from "axios"

export type Config = AxiosRequestConfig;

export type QueryValue = string | number | string[] | number[] | Date | boolean | undefined;
export type Query = Record<string, QueryValue>;
export type QueryTransfomer = (query: Query) => Query;

export type RequestHeaders = AxiosRequestHeaders;
export type RequestTransfomer = AxiosRequestTransformer;

export type ResponseHeaders = AxiosResponseHeaders | RawAxiosResponseHeaders;
export type ResponseTransfomer = AxiosResponseTransformer;

export interface Response<T> {
  headers: ResponseHeaders
  statusCode: number
  statusMessage: string
  response: T
}

export type Data = Record<string, string | number | boolean | object | string[] | number[] | object[]>;

export enum ResponseType {
  json = 'json',
  text = 'text',
}

export interface Resource {
  url: string
  method: string
  query?: Query
  body?: object
  response: object | string
  responseType?: ResponseType
  transformQuery?: QueryTransfomer | QueryTransfomer[]
  transformRequest?: RequestTransfomer | RequestTransfomer[]
  transformResponse?: ResponseTransfomer | RequestTransfomer[]
  validateStatus?: ((status: number) => boolean)
}