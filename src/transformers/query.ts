import { Query, QueryTransfomer, QueryValue } from '../../src/types';


export interface QueryOptionTransformer {
  key?: string
  transformer?: (value: QueryValue) => QueryValue
}

export type QueryOptionsTransformer = Record<string, QueryOptionTransformer>;

export function queryTransformers(transformers: QueryOptionsTransformer): QueryTransfomer {
  return (query: Query): Query => { 
    const res: Query = {};
    for (const [key, value] of Object.entries(query)) {
      if (value === undefined) {
        continue;
      }
      const transformer = transformers[key] ?? {};
      const newKey = transformer.key ?? key;
      const newValue = transformer.transformer !== undefined ? transformer.transformer(value) : value;
      res[newKey] = newValue;
    }
    return res;
  }
}
