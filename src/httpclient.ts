import axios, { AxiosRequestConfig } from 'axios';
import { Query, Response, Resource } from './types';

export class HttpClient {
  protected readonly url: string;
  protected readonly config: AxiosRequestConfig;
  constructor (config: AxiosRequestConfig) {
    this.config = config;
  }

  protected getValue (value: string | number | boolean): string {
    return value.toString();
  }

  protected handleUrl (url: string, query: Query): { url: string, params: Record<string, string | number> } {
    const path = url.split('/');
    const params: Record<string, string | number> = {};
    for (const [key, raw] of Object.entries(query)) {
      if (raw === undefined) {
        continue;
      }
      const value = this.getValue(raw.toString());
      const pathIndex = path.indexOf(':' + key);
      if (pathIndex !== -1) {
        path[pathIndex] = value;
      } else {
        params[key] = value;
      }
    }
    return { url: path.join('/'), params };
  }

  public async resource<T extends Resource> (type: new () => T, query: T["query"] | null = null, data: T["body"] | null = null): Promise<Response<T["response"]>> {
    const resource = new type();
    if (query !== null && resource.transformQuery !== undefined) {
      const transformers = Array.isArray(resource.transformQuery) ? resource.transformQuery : [resource.transformQuery];
      for (const transformer of transformers) {
        query = transformer(query);
      }
    }

    const { url, params } = this.handleUrl(resource.url, query ?? {});

    const res = await axios.request<T["response"]>({
      ...this.config,
      url,
      method: resource.method,
      transformRequest: resource.transformRequest,
      transformResponse: resource.transformResponse,
      data,
      params,
      validateStatus: resource.validateStatus,
      responseType: resource.responseType ?? 'json',
    });
    return {
      headers: res.headers,
      statusCode: res.status,
      statusMessage: res.statusText,
      response: res.data
    }
  }
}
