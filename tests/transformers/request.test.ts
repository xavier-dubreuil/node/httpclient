import { expect, it } from '@jest/globals';
import { formData } from '../../src/transformers/request';
import FormData from 'form-data';

it('formData', async () => {
  const input = {
    'name': 'test',
    'value': 'content',
  }
  const data = formData(input);
  expect(data).toBeInstanceOf(FormData);
});
