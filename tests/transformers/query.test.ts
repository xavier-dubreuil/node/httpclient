import { expect, it } from '@jest/globals';
import { QueryOptionsTransformer, queryTransformers } from '../../src/transformers/query';

it('queryTransformer', async () => {
  const changes: QueryOptionsTransformer = {
    id: {
      key: 'uid',
    },
    status: {
      transformer: (value: string[]) => value.join('|'),
    },
    all: {
      key: 'every',
      transformer: () => 'test',
    }
  }

  const transformer = queryTransformers(changes);
  const input = {
    id: 234,
    status: ['val1', 'val2'],
    all: 'something',
    other: 'null',
    null: undefined,
  };
  const check = {
    uid: 234,
    status: 'val1|val2',
    every: 'test',
    other: 'null',
  }
  const output = transformer(input);
  expect(output).toEqual(check);
});
