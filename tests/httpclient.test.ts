import { expect, it } from '@jest/globals';
import { HttpClient, Query, Resource, ResponseType } from '../src';
import * as fixtures from '../fixtures';

it('Standard', async () => {
  class Test implements Resource {
    url = fixtures.axios.standard.standardHttpsCall.url
    method = 'get'
    response: string
    responseType = ResponseType.text
  }
  const client = new HttpClient({ baseURL: fixtures.axios.standard.host });
  const result = await client.resource(Test);
  expect(result).not.toBeNull();
  if (result !== null) {
    expect(result.response).toEqual(fixtures.axios.standard.standardHttpsCall.body);
  }
});

it('post', async () => {
  class Test implements Resource {
    url = fixtures.axios.standard.standardHttpsCall.url
    method = 'post'
    response: string
    responseType = ResponseType.text
  }
  const client = new HttpClient({ baseURL: fixtures.axios.standard.host });
  const result = await client.resource(Test, {}, { key: 'value' });
  expect(result).not.toBeNull();
  if (result !== null) {
    expect(result.response).toEqual(fixtures.axios.standard.standardHttpsCall.body);
  }
});

it('Standard param undefined', async () => {
  class Test implements Resource {
    url = fixtures.axios.standard.standardHttpsCall.url
    method = 'get'
    response: string
    responseType = ResponseType.text
  }
  const client = new HttpClient({ baseURL: fixtures.axios.standard.host });
  const result = await client.resource(Test, { query: undefined });
  expect(result).not.toBeNull();
  if (result !== null) {
    expect(result.response).toEqual(fixtures.axios.standard.standardHttpsCall.body);
  }
});

it('Url parameters call', async () => {
  class Test implements Resource {
    url = '/domain/:id/action'
    method = 'get'
    query: {
      id: number,
      status: string,
    }
    response: string
    responseType = ResponseType.text
  }
  const client = new HttpClient({ baseURL: fixtures.axios.standard.host });
  const result = await client.resource(Test, { id: 42, status: 'active' });
  expect(result).not.toBeNull();
  if (result !== null) {
    expect(result.response).toEqual(fixtures.axios.standard.paramsUrl.body);
  }
});

it('Url parameters call with transformer', async () => {
  class Test implements Resource {
    url = '/domain/:id/action'
    method = 'get'
    query: {
      id: number,
      status: string,
    }
    response: string
    responseType = ResponseType.text
    transformQuery = (): Query => { return { id: 42, status: 'active' } };
  }
  const client = new HttpClient({ baseURL: fixtures.axios.standard.host });
  const result = await client.resource(Test, { id: 43, status: 'null' });
  expect(result).not.toBeNull();
  if (result !== null) {
    expect(result.response).toEqual(fixtures.axios.standard.paramsUrl.body);
  }
});

it('Url parameters call with transformers', async () => {
  class Test implements Resource {
    url = '/domain/:id/action'
    method = 'get'
    query: {
      id: number,
      status: string,
    }
    response: string
    responseType = ResponseType.text
    transformQuery = [
      (): Query => { return { id: 42 } },
      (query: Query): Query => { return { ...query, status: 'active' } }
    ];
  }
  const client = new HttpClient({ baseURL: fixtures.axios.standard.host });
  const result = await client.resource(Test, { id: 43, status: 'null' });
  expect(result).not.toBeNull();
  if (result !== null) {
    expect(result.response).toEqual(fixtures.axios.standard.paramsUrl.body);
  }
});

// it('Error Status code', async () => {
//   const client = new HttpClient(fixtures.https.standard.host, {});
//   const result = await client.call('GET', fixtures.https.standard.errorStatusCode.url, {})
//   expect(result.statusCode).toEqual(404);
// });

// it('timeout', async () => {
//   const client = new HttpClient(fixtures.https.standard.host, {});
//   await client.call('GET', fixtures.https.standard.timeout.url, {}).catch(e => {
//     expect(e.message).toEqual('Timeout');
//   })
// });

// it('error', async () => {
//   const client = new HttpClient(fixtures.https.standard.host, {});
//   await client.call('GET', fixtures.https.standard.error.url, {}).catch(e => {
//     expect(e.message).toEqual(fixtures.https.standard.error.error);
//   });
// });

// it ('Json call', async () => {
//   const client = new HttpClient(fixtures.https.standard.host, {});
//   const result = await client.json('GET', fixtures.https.standard.jsonHttpCall.url);
//   expect(result).not.toBeNull();
//   if (result !== null) {
//     expect(result.response).toEqual(JSON.parse(fixtures.https.standard.jsonHttpCall.body as string));
//   }
// });

// it ('Json empty', async () => {
//   const client = new HttpClient(fixtures.https.standard.host, {});
//   await client.json('GET', fixtures.https.standard.errorStatusCode.url).catch(e => {
//     expect(e).toBeInstanceOf(SyntaxError);
//     expect(e.message).toEqual('Unexpected end of JSON input');
//   });
// });

it('Resource call', async () => {
  interface Resp {
    success: boolean,
    content: string,
  }
  class Test implements Resource {
    url = fixtures.axios.standard.jsonHttpCall.url
    method = 'get'
    response: Resp
  }
  const client = new HttpClient({ baseURL: fixtures.axios.standard.host });
  const result = await client.resource(Test, {});
  expect(result).not.toBeNull();
  if (result !== null) {
    expect(result.response).toEqual(JSON.parse(fixtures.axios.standard.jsonHttpCall.body as string));
  }
});
