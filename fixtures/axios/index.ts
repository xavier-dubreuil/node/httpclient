import {default as standard} from './standard';
export * as standard from './standard';

export const actions = [
  ...standard,
];
