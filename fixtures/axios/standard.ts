import * as mocks from '../../__mocks__';

export const host = 'https://localhost';

export const standardHttpsCall: mocks.axios.ActionInput = {
  url: '/standard-https-call',
  body: 'https-content',
  params: []
};

export const paramsUrl: mocks.axios.ActionInput = {
  url: '/domain/42/action',
  body: 'result content',
  params: [{
    field: 'status',
    type: 'exact',
    value: 'active'
  }]
};

export const jsonHttpCall: mocks.axios.ActionInput = {
  url: '/json',
  body: JSON.stringify({ success: true, content: 'default' }),
  params: []
};

export const errorStatusCode: mocks.axios.ActionInput = {
  url: '/errorStatusCode',
  statusCode: 404,
  params: []
};

export const timeout: mocks.axios.ActionInput = {
  url: '/timeout',
  params: [],
  timeout: true
};

export const error: mocks.axios.ActionInput = {
  url: '/error',
  params: [],
  error: 'Bad Request'
};

export default [standardHttpsCall, paramsUrl, jsonHttpCall, errorStatusCode, timeout, error].map(item => {
  return { ...item, url: host + item.url };
}) as mocks.axios.ActionInput[];
